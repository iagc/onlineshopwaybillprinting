﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopWaybillPrinting
{
    public static class Globals
    {
		public enum DataDirection
		{
			First, 
			Previous, 
			Next, 
			Last
		};

        public const string ShipmentQuery =
			@" SELECT
				s.order_id InternalOrderId, s.order_increment_id OrderId, s.order_created_at OrderCreatedAt, s.created_at ShipmentCreatedAt, s.shipping_name ShippingName,
				s.total_qty TotalQty, s.shipping_address ShippingAddress, s.shipping_information ShippingInformation, o.grand_total GrandTotal,
				o.payment_method PaymentMethod,
				a.telephone Telephone, a.email Email
			FROM
				sales_shipment_grid s
			INNER JOIN
				sales_order_grid o
			ON
				o.entity_id = s.order_id
			INNER JOIN
				sales_order_address a
			ON
				a.parent_id = o.entity_id
			WHERE
				o.`status` = 'waitingf_for_courier'
			AND
				a.address_type = 'shipping'";

    }
}
