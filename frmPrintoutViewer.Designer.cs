﻿namespace OnlineShopWaybillPrinting
{
    partial class frmPrintoutViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.PrintoutTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Printout = new OnlineShopWaybillPrinting.Printout();
            this.PrintoutReportviewer = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.PrintoutTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Printout)).BeginInit();
            this.SuspendLayout();
            // 
            // PrintoutTableBindingSource
            // 
            this.PrintoutTableBindingSource.DataMember = "PrintoutTable";
            this.PrintoutTableBindingSource.DataSource = this.Printout;
            // 
            // Printout
            // 
            this.Printout.DataSetName = "Printout";
            this.Printout.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // PrintoutReportviewer
            // 
            this.PrintoutReportviewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Printout";
            reportDataSource1.Value = this.PrintoutTableBindingSource;
            this.PrintoutReportviewer.LocalReport.DataSources.Add(reportDataSource1);
            this.PrintoutReportviewer.LocalReport.ReportEmbeddedResource = "OnlineShopWaybillPrinting.Printout.rdlc";
            this.PrintoutReportviewer.Location = new System.Drawing.Point(0, 0);
            this.PrintoutReportviewer.Name = "PrintoutReportviewer";
            this.PrintoutReportviewer.ServerReport.BearerToken = null;
            this.PrintoutReportviewer.Size = new System.Drawing.Size(643, 450);
            this.PrintoutReportviewer.TabIndex = 0;
            // 
            // frmPrintoutViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 450);
            this.Controls.Add(this.PrintoutReportviewer);
            this.Name = "frmPrintoutViewer";
            this.Load += new System.EventHandler(this.PrintoutViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PrintoutTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Printout)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer PrintoutReportviewer;
        private System.Windows.Forms.BindingSource PrintoutTableBindingSource;
        private Printout Printout;
    }
}