﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using MySql.Data.MySqlClient;

namespace OnlineShopWaybillPrinting
{
    
    public partial class frmMain : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int Offset { get; set; }
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            
            if (gridShipments.DataSource != null)
            {
                if (gridShipments.SelectedRows[0] == null)
                    return;

                var source = new BindingSource();
                source = (BindingSource)gridShipments.DataSource;
                var currentlyLoadedData = (List<SalesShipment>)source.DataSource;

                if (!int.TryParse(gridShipments.SelectedRows[0].Cells[0].Value.ToString(),
                    out int internalOrderId))
                    return;

                var result = from c in currentlyLoadedData
                             where c.InternalOrderId == internalOrderId
                             select c;

                var f = new frmPrintoutViewer(result);
                f.ShowDialog();
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void LoadData(Globals.DataDirection dataDirection)
        {
            var source = new BindingSource();            

            btnLoad.Enabled = false;
            btnFirst.Enabled = false;
            btnPrevious.Enabled = false;
            btnNext.Enabled = false;
            btnLast.Enabled = false;

            var fromDate = new DateTime(dtStart.Value.Year, dtStart.Value.Month, dtStart.Value.Day, 0, 0, 0);
            var toDate = new DateTime(dtEnd.Value.Year, dtEnd.Value.Month, dtEnd.Value.Day, 23, 59, 59);

            switch (dataDirection)
            {
                case Globals.DataDirection.First:
                    Offset = 0;
                    break;
                case Globals.DataDirection.Previous:
                    Offset -= 50;
                    break;
                case Globals.DataDirection.Next:
                    Offset += 50;
                    break;
                case Globals.DataDirection.Last:
                    break;
                default:
                    break;
            }

            Offset = Offset < 0 ? 0 : Offset;

            var shipments = await GetShipments(fromDate, toDate, Offset);

            if (shipments == null)
            {
                MessageBox.Show("No Data to Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btnLoad.Enabled = true;
                btnLoad.Enabled = true;
                btnFirst.Enabled = true;
                btnPrevious.Enabled = true;
                btnNext.Enabled = true;
                btnLast.Enabled = true;
                return;
            }

            source.DataSource = shipments;
            gridShipments.DataSource = source;
            btnLoad.Enabled = true;
            btnFirst.Enabled = true;
            btnPrevious.Enabled = true;
            btnNext.Enabled = true;
            btnLast.Enabled = true;
        }

        private async Task<List<SalesShipment>> GetShipments(DateTime fromDate, DateTime toDate, int Offset)
        {

            StringBuilder sql = new StringBuilder(Globals.ShipmentQuery);

            if (fromDate != null && toDate != null)
            {
                sql.Append($" AND s.order_created_at >= '{fromDate.ToString("yyyy-MM-dd HH:mm:ss")}'");
                sql.Append($" AND s.order_created_at <= '{toDate.ToString("yyyy-MM-dd HH:mm:ss")}'");
            }

            sql.Append(" LIMIT ");

            if (Offset != 0)
            {
                sql.Append($" {Offset}, ");
            }

            sql.Append(" 50");

            log.Info(sql);

            var shipments = new List<SalesShipment>();

            using (IDbConnection db = new MySqlConnection(ConfigurationManager.ConnectionStrings["tonymoly"].ConnectionString))
            {
                var result = await Task.Run(()
                    => db.QueryAsync<SalesShipment>(sql.ToString()));

                if (result.Any())
                {
                    return result.ToList();
                }
            }

            return null;
        }
        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadData(Globals.DataDirection.First);
        }
        private void btnFirst_Click(object sender, EventArgs e)
        {
            if (gridShipments.DataSource != null)
                LoadData(Globals.DataDirection.First);
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if (gridShipments.DataSource != null)
                LoadData(Globals.DataDirection.Previous);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if (gridShipments.DataSource != null)
                LoadData(Globals.DataDirection.Next);
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            if (gridShipments.DataSource != null)
                LoadData(Globals.DataDirection.Last);
        }

        private void gridShipments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            LoadDetail();
        }

        private void LoadDetail()
        {
            if (gridShipments.DataSource != null)
            {
                if (gridShipments.SelectedRows[0] == null)
                    return;

                var source = new BindingSource();
                source = (BindingSource)gridShipments.DataSource;
                var currentlyLoadedData = (List<SalesShipment>)source.DataSource;

                if (!int.TryParse(gridShipments.SelectedRows[0].Cells[0].Value.ToString(), 
                    out int internalOrderId))
                    return;
                
                var result = from c in currentlyLoadedData
                             where c.InternalOrderId == internalOrderId
                             select c;

                var row = result.FirstOrDefault();
                frmDetail f = new frmDetail
                {
                    Shipment = row
                };
                f.ShowDialog();

            }

        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            LoadDetail();
        }
    }
}
