﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopWaybillPrinting
{
    public class SalesShipment
    {
        public int InternalOrderId { get; set; }
        public string OrderId { get; set; }
        public DateTime OrderCreatedAt { get; set; }
        public DateTime ShipmentCreatedAt { get; set; }
        public string ShippingName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }        
        public string ShippingAddress { get; set; }
        public string ShippingInformation { get; set; }
        public string PaymentMethod { get; set; }
        public int TotalQty { get; set; }
        public double GrandTotal { get; set; }
        

    }
}
