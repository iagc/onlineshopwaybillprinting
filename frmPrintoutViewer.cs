﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineShopWaybillPrinting
{
    public partial class frmPrintoutViewer : Form
    {
        public IEnumerable<SalesShipment> SalesShipments { get; internal set; }
        public frmPrintoutViewer(IEnumerable<SalesShipment> salesShipments)
        {
            InitializeComponent();
            SalesShipments = salesShipments;
        }

        private void PrintoutViewer_Load(object sender, EventArgs e)
        {
            this.Text = $"Order No. {SalesShipments.FirstOrDefault().OrderId}";
            var source = new BindingSource();
            source.DataSource = SalesShipments;
            this.PrintoutReportviewer.LocalReport.DataSources.Clear();

            var rdt = new ReportDataSource("Printout", source);
            if (SalesShipments.FirstOrDefault().ShippingInformation.Like("%CliqNShip%"))
            {
                this.PrintoutReportviewer.LocalReport.ReportPath = "PrintoutCliqNShip.rdlc";
            }
            else if (SalesShipments.FirstOrDefault().ShippingInformation.Like("%Fastrack%"))
            {
                this.PrintoutReportviewer.LocalReport.ReportPath = "PrintoutFastrack.rdlc";
            }
            else
            {
                this.PrintoutReportviewer.LocalReport.ReportPath = "PrintoutLBC.rdlc";
            }

            this.PrintoutReportviewer.LocalReport.EnableExternalImages = true;
            this.PrintoutReportviewer.LocalReport.DataSources.Add(rdt);

            ReportParameter[] params1 = new ReportParameter[3];
            params1[0] = new ReportParameter("SenderName", "TONYMOLY Philippines Inc.", false);
            params1[1] = new ReportParameter("SenderAddress", "5148 Filmore St. Brgy. Palanan Makati City", false);
            params1[2] = new ReportParameter("SenderPhone", "88227116", false);
            this.PrintoutReportviewer.LocalReport.SetParameters(params1);

            this.PrintoutReportviewer.RefreshReport();
        }
    }
}
