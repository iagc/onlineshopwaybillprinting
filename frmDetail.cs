﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineShopWaybillPrinting
{
    public partial class frmDetail : Form
    {
        public SalesShipment Shipment { get; set; }
        public frmDetail()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDetail_Load(object sender, EventArgs e)
        {
            txtOrderId.Text = Shipment.OrderId;
            txtOrderCreatedAt.Text = Shipment.OrderCreatedAt.ToString();
            txtShipmentCreatedAt.Text = Shipment.ShipmentCreatedAt.ToString();
            txtShippingName.Text = Shipment.ShippingName;
            txtTelephone.Text = Shipment.Telephone;
            txtEmail.Text = Shipment.Email;
            txtShippingAddress.Text = Shipment.ShippingAddress;
            txtShippingInformation.Text = Shipment.ShippingInformation;
            txtPaymentMethod.Text = Shipment.PaymentMethod;
            txtTotalQty.Text = Shipment.TotalQty.ToString("N0");
            txtGrandTotal.Text = Shipment.GrandTotal.ToString("N2");
            
        }
    }
}
